/*
- Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/
	const getCube = (num) => {
		const cubeOfNumber = Math.pow(num, 3);
		console.log(`The cube of ${num} is ${cubeOfNumber}`);
	}

	getCube(2);
	

/*
- Create a variable address with a value of an array containing details of an address.
- Destructure the array and print out a message with the full address using Template Literals.
*/
	const myAddress = ["Bayanan", "Muntinlupa", "Metro Manila"];
	const [barangay, city, province] = myAddress;

	/*console.log(barangay);
	console.log(city);
	console.log(province);

	console.log(myAddress);*/

	console.log(`I am from ${barangay}, ${city} City, ${province}, Philippines`);


/*
- Create a variable animal with a value of an object data type with different animal details as its properties.
	- name
	- species
	- weight
	- measurement
- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	const animal = {
		name: "Lolong",
		species: "Saltwater",
		weight: "1075 kgs",
		measurement: "20ft 3in",
	}

	const {name, species, weight, measurement} = animal
	console.log(`${name} was a ${species} crocodile. He weighed ${weight} with a measurement of ${measurement}.`);


/*
- Create an array of numbers.
- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
- Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/
	const numbers = [1, 2, 3, 4, 5];

	numbers.forEach((num) => {
		console.log(num);
	})

	const reduceNumber = numbers.reduce((a, b) => a + b);
	console.log(reduceNumber);


/*
- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
- Create/instantiate a new object from the class Dog and console log the object.
*/
	class Dog {
		constructor(dogName, dogAge, dogBreed) {
			this.name = dogName;
			this.age = dogAge;
			this.breed = dogBreed;
		}
	}

	let dog = new Dog("Frankie", 5, "Miniature Dachshund");
	console.log(dog);







